# baidusearch_crawl
crawl time and title from baidusearch on timeline
_____________________________________________________
Requirements:

1.python3 on Anaconda
2.scrapy 1.4
_____________________________________________________
Tips:

1.before start crawl,please choose proper time-line under the function 
Start_request in spider.py;
2.the results of the crawl only print on shell,if you want to save data 
in databases or files,please replace function print with yield and modify
the codes in spdier.py and items.py;
3.the crawl only can collect the first page of search result,if you want to 
crawl more pages,please add a page turning function.
_____________________________________________________
Matters:

beacause of the encryption algorithm on time-line,I only write a fixed time in url,
which lack of flexibility.